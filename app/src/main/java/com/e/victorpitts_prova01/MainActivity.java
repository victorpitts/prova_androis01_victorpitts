package com.e.victorpitts_prova01;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.e.victorpitts_prova01.resp_sal;

public class MainActivity extends AppCompatActivity {
    Button p40,p45,p50;
    double res_sal;
    EditText sal;

    public void setRessal(double r){
        this.res_sal = r;
    }
    public double getRes_sal(){
        return this.res_sal;
    }

    private AlertDialog alerta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        p40 = findViewById(R.id.p_40);
        p45 = findViewById(R.id.p_45);
        p50 = findViewById(R.id.p_50);
        sal = findViewById(R.id.salario);
        p40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRessal(Float.parseFloat(String.valueOf(sal.getText())));

                //AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Aumento Salarial");
                double sal_m_alm = Math.round((getRes_sal()+((getRes_sal()*40)/100)));
//                resp_sal re = new resp_sal();
//                re.setRes("Novo Salário = "+sal_m_alm);
//                Intent resp_sal_ = new Intent(getApplicationContext(),resp_sal.class);
//                startActivity(resp_sal_);

                builder.setMessage(String.valueOf("Novo Salário = "+sal_m_alm));
                alerta = builder.create();
                alerta.show();

            }
        });
        p45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRessal(Float.parseFloat(String.valueOf(sal.getText())));
//                Intent resp_sal_ = new Intent(getApplicationContext(),resp_sal.class);
//                startActivity(resp_sal_);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Aumento Salarial");
                double sal_m_alm = Math.round((getRes_sal()+((getRes_sal()*45)/100)));
                builder.setMessage(String.valueOf("Novo Salário = "+sal_m_alm));
                alerta = builder.create();
                alerta.show();

            }
        });
        p50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRessal(Float.parseFloat(String.valueOf(sal.getText())));
//                Intent resp_sal_ = new Intent(getApplicationContext(),resp_sal.class);
//                startActivity(resp_sal_);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Aumento Salarial");
                double sal_m_alm = Math.round((getRes_sal()+((getRes_sal()*50)/100)));
                builder.setMessage(String.valueOf("Novo Salário = "+sal_m_alm));
                alerta = builder.create();
                alerta.show();

            }
        });

    }
}
