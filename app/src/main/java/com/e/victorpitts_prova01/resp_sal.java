package com.e.victorpitts_prova01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class resp_sal extends AppCompatActivity {
    private String res;
    public void setRes(String r){
        this.res = r;
    }

    public String getRes() {
        return res;
    }
    TextView resposta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resp_sal);
        resposta = findViewById(R.id.resp_sal_r);
        resposta.setText(getRes().toString());
    }
}
